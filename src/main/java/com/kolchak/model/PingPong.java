package com.kolchak.model;

import java.time.LocalDateTime;

public class PingPong {
    private volatile static long A = 0;
    private static Object object = new Object();

    public void startGame() {
        Thread thread1 = new Thread(() -> {
            synchronized (object) {
                for (int i = 1; i <= 100000; i++) {
                    try {
                        object.wait();
                    } catch (InterruptedException e) {
                    }
                    A++;
                    object.notify();
                }
                System.out.println("Finish" + Thread.currentThread().getName());
            }
        });

        Thread thread2 = new Thread(() -> {
            synchronized (object) {
                for (int i = 1; i <= 100000; i++) {
                    object.notify();
                    try {
                        object.wait();
                    } catch (InterruptedException e) {
                    }
                    A++;
                }
                System.out.println("Finish" + Thread.currentThread().getName());
            }
        });
        System.out.println(LocalDateTime.now());
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
        }
        System.out.println(LocalDateTime.now());
        System.out.println("A: " + A);

    }
}
