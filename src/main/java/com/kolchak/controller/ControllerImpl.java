package com.kolchak.controller;

import com.kolchak.model.PingPong;

public class ControllerImpl implements Controller {
    PingPong pingPong;
    @Override
    public void pingPongTest() {
        pingPong = new PingPong();
        pingPong.startGame();
    }
}
